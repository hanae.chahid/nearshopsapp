import React from 'react'
import axios from 'axios'
import classnames from 'classnames'

class Register extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            errors: {}
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange = (e) => {
        this.setState({ 
            [e.target.name]: e.target.value
        })
    }

    onSubmit = (e) => {
        e.preventDefault();
        const user = {
            email: this.state.email,
            password: this.state.password
        };
        axios.post('/api/users/register',user)
            .then(result => {
                console.log(result);
            })
            .catch(err => this.setState({
                errors: err.response.data
            }));
    }

    render(){
        const { errors} = this.state;
        return (
            <div className="register ">
                <div className="container">
                    <div className="row">
                        <div className="col-8 m-auto">
                            <h1 className="display-4 text-center">Sign Up</h1>
                            <p className="lead text-center">Create your own account</p>
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <input
                                        className={classnames('form-control form-control-lg', {
                                            'is-invalid': errors.email
                                        })} 
                                        placeholder="Email Address" name="email" 
                                        value={this.state.email}
                                        onChange={this.onChange}  />
                                    {
                                        errors.email && (<div className="invalid-feedback">{errors.email}</div>)
                                    }
                                </div>
                                <div className="form-group">
                                    <input type="password" 
                                        className={classnames('form-control form-control-lg', {
                                            'is-invalid': errors.password
                                        })} 
                                        placeholder="Password" name="password"
                                        value={this.state.password}
                                        onChange={this.onChange} />
                                    {
                                        errors.password && (<div className="invalid-feedback">{errors.password}</div>)
                                    }
                                </div>
                                <input type="submit" className="btn btn-info btn-block mt-4" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Register;
