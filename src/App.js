import React, { Component } from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import NavBar from './components/layouts/NavBar';
import Landing from './components/layouts/landing';
import Register from './components/auth/register';
import Login from './components/auth/login';

import './App.css';

class App extends Component {
  render() {
    return (
        <Router>
          <div>
            <NavBar />
            <Route exact path="/" component={ Landing } />
            <Route path="/register" component={ Register } />
            <Route path="/login" component={ Login } />
          </div>
        </Router> 
    )
  }
}

export default App;
